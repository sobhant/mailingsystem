<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Messaages
 *
 * @ORM\Table(name="messages")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MessaagesRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Messages
{
    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="message", type="text")
     */
    private $message;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_from_id", referencedColumnName="id")
     */
    public $From;

    /**
     * @var integer
     * @ORM\Column(name="draft_status", type="integer")
     */
    public $draftStatus = 0;

    /**
     * @ORM\ManyToMany(targetEntity="User")
     * @ORM\JoinTable(name="recipient")
     */
    private $To;

    /**
     * @var datetime
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;    


    /**
     * Get id
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set message
     * @param string $message
     * @return Messaages
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @ORM\PrePersist
     */
    public function updatedTimestamps()
    {
        $this->setCreatedAt(new \DateTime('now'));
    }

    /**
     * Set createdAt
     * @param \DateTime $createdAt
     * @return Messaages
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    
    /**
     * Set from
     * @param \AppBundle\Entity\User $from
     * @return Messages
     */
    public function setFrom(\AppBundle\Entity\User $from = null)
    {
        $this->From = $from;

        return $this;
    }

    /**
     * Get from
     * @return \AppBundle\Entity\User
     */
    public function getFrom()
    {
        return $this->From;
    }
    
    public function __construct()
    {
        $this->To = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add to
     * @param \AppBundle\Entity\User $to
     * @return Messages
     */
    public function addTo(\AppBundle\Entity\User $to)
    {
        $this->To[] = $to;

        return $this;
    }

    /**
     * Remove to
     * @param \AppBundle\Entity\User $to
     */
    public function removeTo(\AppBundle\Entity\User $to)
    {
        $this->To->removeElement($to);
    }

    /**
     * Get to
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTo()
    {
        return $this->To;
    }



    /**
     * Set draftStatus
     * @param integer $draftStatus
     * @return Messages
     */
    public function setDraftStatus($draftStatus)
    {
        $this->draftStatus = $draftStatus;

        return $this;
    }

    /**
     * Get draftStatus
     * @return integer
     */
    public function getDraftStatus()
    {
        return $this->draftStatus;
    }
}
