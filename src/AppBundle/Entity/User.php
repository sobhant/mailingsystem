<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity
 * @UniqueEntity(fields="email", message="Email already taken")
 * @UniqueEntity(fields="username", message="Username already taken")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    private $email;


    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    private $recoveryemail;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @Assert\NotBlank()
     */
    private $username;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(max=4096)
     */
    private $plainPassword;

    /**
     * The below length depends on the "algorithm" you use for encoding.
     * @ORM\Column(type="string", length=64)
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    private $resetToken;


    /**
     * @ORM\ManyToOne(targetEntity="SecurityQuestions")
     * @ORM\JoinColumn(name="security_question_id", referencedColumnName="id")
     */
    protected $security_question;

    /**
     * @ORM\Column(type="string", length=64)
     */
    protected $security_answer;
    
    /**
     * @ORM\Column(type="integer")
     */
    protected $login_attempt = 0;

    /**
     * @ORM\Column(type="integer")
     */
    protected $block_status = 0;


    /**
     * @ORM\Column(type="array")
     */
    private $roles;

    public function __construct()
    {
        $this->roles = array('ROLE_USER');
    }

    // other properties and methods

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function setUsername($username)
    {
        $this->username = $username;
    }

    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    public function setPlainPassword($password)
    {
        $this->plainPassword = $password;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function getSalt()
    {
        // The bcrypt and argon2i algorithms don't require a separate salt.
        // You *may* need a real salt if you choose a different encoder.
        return null;
    }

    public function getRoles()
    {
        return $this->roles;
    }

    public function eraseCredentials()
    {
    }

    /**
     * Get id
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set roles
     * @param array $roles
     *
     * @return User
     */
    public function setRoles($roles)
    {
        $this->roles = $roles;

        return $this;
    }


    
    /**
     * Set securityAnswer
     *
     * @param string $securityAnswer
     *
     * @return User
     */
    public function setSecurityAnswer($securityAnswer)
    {
        $this->security_answer = $securityAnswer;

        return $this;
    }

    /**
     * Get securityAnswer
     *
     * @return string
     */
    public function getSecurityAnswer()
    {
        return $this->security_answer;
    }

    /**
     * Set securityQuestion
     *
     * @param \AppBundle\Entity\SecurityQuestions $securityQuestion
     *
     * @return User
     */
    public function setSecurityQuestion(\AppBundle\Entity\SecurityQuestions $securityQuestion = null)
    {
        $this->security_question = $securityQuestion;

        return $this;
    }

    /**
     * Get securityQuestion
     *
     * @return \AppBundle\Entity\SecurityQuestions
     */
    public function getSecurityQuestion()
    {
        return $this->security_question;
    }


    /**
     * Set loginAttempt
     *
     * @param integer $loginAttempt
     *
     * @return User
     */
    public function setLoginAttempt($loginAttempt)
    {
        $this->login_attempt = $loginAttempt;

        return $this;
    }

    /**
     * Get loginAttempt
     *
     * @return integer
     */
    public function getLoginAttempt()
    {
        return $this->login_attempt;
    }

    /**
     * Set resetToken
     *
     * @param string $resetToken
     *
     * @return User
     */
    public function setResetToken($resetToken)
    {
        $this->resetToken = $resetToken;

        return $this;
    }

    /**
     * Get resetToken
     *
     * @return string
     */
    public function getResetToken()
    {
        return $this->resetToken;
    }

    /**
     * Set recoveryemail
     *
     * @param string $recoveryemail
     *
     * @return User
     */
    public function setRecoveryemail($recoveryemail)
    {
        $this->recoveryemail = $recoveryemail;

        return $this;
    }

    /**
     * Get recoveryemail
     *
     * @return string
     */
    public function getRecoveryemail()
    {
        return $this->recoveryemail;
    }

    /**
     * Set blockStatus
     *
     * @param integer $blockStatus
     *
     * @return User
     */
    public function setBlockStatus($blockStatus)
    {
        $this->block_status = $blockStatus;

        return $this;
    }

    /**
     * Get blockStatus
     *
     * @return integer
     */
    public function getBlockStatus()
    {
        return $this->block_status;
    }
}
