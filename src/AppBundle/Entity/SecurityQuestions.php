<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SecurityQuestions
 *
 * @ORM\Table(name="security_questions")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SecurityQuestionsRepository")
 */
class SecurityQuestions
{
    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="question", type="string", length=255)
     */
    protected $question;


    /**
     * Get id
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set question
     * @param string $question
     * @return SecurityQuestions
     */
    public function setQuestion($question)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question
     * @return string
     */
    public function getQuestion()
    {
        return $this->question;
    }

}
