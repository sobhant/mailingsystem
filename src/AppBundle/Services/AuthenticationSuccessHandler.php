<?php

namespace AppBundle\Services;

use AppBundle\Entity\User;
use Symfony\Component\Security\Http\Authentication\DefaultAuthenticationSuccessHandler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Http\HttpUtils;
use AppBundle\Services\UserServices;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Session\Session;

class AuthenticationSuccessHandler extends DefaultAuthenticationSuccessHandler
{
    private $container;
    private $userservices;
    private $em;


    function __construct(HttpUtils $httpUtils,UserServices $userservices, EntityManager $em, ContainerInterface $container)
    {
        parent::__construct($httpUtils);
        $this->userservices = $userservices;
        $this->container = $container;
        $this->em = $em;
    }
    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        $session = new Session();
        $username = $request->get('_username');
        $user = $this->userservices->GetUser($username);
        if($user->getBlockStatus() != 0) {
            $session->getFlashBag()->set('loginError','User is Blocked');                
            $url = $this->container->get('router')->generate('login');
            return new RedirectResponse($url);
        }
        $this->userservices->SetLoginAttempt($user,0);
        $url = $this->container->get('router')->generate('message_layout');
        return new RedirectResponse($url);
    }
}