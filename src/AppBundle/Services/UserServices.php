<?php
namespace AppBundle\Services;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;


class UserServices
{
    private $container;
    protected $em;
    public function __construct(EntityManager $entityManager,ContainerInterface $container)
    {
        $this->em = $entityManager;
        $this->container = $container;
    }

    public function GetUser($username)
    {
        try
        {
            $em = $this->em;

            // Search whether the voucher is present or not.
            // If the Voucher is not present then return an error message.
            $user = $em->getRepository('AppBundle:User')->findOneBy(array('username' => $username));

            if(!empty($user))
            {
                return $user;
            }

            return null;
        }

        catch(Exception $e)
        {
            throw new Exception($e->getMessage());
        }
    }

    public function SetLoginAttempt($user,$attempt)
    {
        $user->setLoginAttempt($attempt);
        $this->em->flush();
    }
    public function isPasswordSame($user, $password)
    {
        $encoderService = $this->container->get('security.password_encoder');
        $match = $encoderService->isPasswordValid($user, $password);
       return $match;
    }

    public function SetBlockStatus($user, $status)
    {
        $user->setBlockStatus($status);
        $this->em->flush();
    }

}