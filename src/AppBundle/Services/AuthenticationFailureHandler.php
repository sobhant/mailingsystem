<?php

namespace AppBundle\Services;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\Authentication\DefaultAuthenticationFailureHandler;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Session;
use AppBundle\Services\UserServices;

class AuthenticationFailureHandler extends DefaultAuthenticationFailureHandler
{
    private $container;
    private $userservices;

    public function __construct(HttpKernelInterface $httpKernel, ContainerInterface $container, UserServices $userservices)
    {
        $this->httpKernel = $httpKernel;
        $this->container = $container;
        $this->userservices = $userservices;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $session = new Session();
        $username = $request->get('_username');
        $user = $this->userservices->GetUser($username);

        if($user)
        {
            $attempt = $user->getLoginAttempt();
            $attempt = $attempt+1;
            
            $session->getFlashBag()->set('loginError',(5-$attempt).' Attempts left');

            if($attempt >=5)
            {
                $attempt = 5;
                $session->getFlashBag()->set('loginError','User is Blocked');                
                $this->userservices->SetLoginAttempt($user,$attempt);
                $this->userservices->SetBlockStatus($user,1);
                $url=$this->container->get('router')->generate('forgot_password');
                return new RedirectResponse($url);
            }
            $this->userservices->SetLoginAttempt($user,$attempt);
        }
        else
        {
            $session->getFlashBag()->set('loginError','Invalid Login Details');
        }

        $url=$this->container->get('router')->generate('login');
        return new RedirectResponse($url);
    }
}