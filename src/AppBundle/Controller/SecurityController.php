<?php
namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use AppBundle\Form\ResetType;
use AppBundle\Form\ResetPassword;
use AppBundle\Entity\User;
use Symfony\Component\HttpFoundation\Session\Session;

class SecurityController extends Controller
{
	/**
     * @Route("/login", name="login")
     */
    public function login(AuthenticationUtils $authenticationUtils, Request $request)
	{
	    // last username entered by the user
	    $lastUsername = $authenticationUtils->getLastUsername();

	    return $this->render('AppBundle:User:login.html.twig', array(
	        'last_username' => $lastUsername));
	}

	/**
     * @Route("/forgot-password", name="forgot_password")
     */
    public function forgotPasswordAction(Request $request)
    {
        $session = new Session();
        $updateUser = new User();
        $form = $this->createForm(ResetType::class, $updateUser);

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            $email = $form->getData()->getEmail();
            $entityManager = $this->getDoctrine()->getManager();
            $user = $entityManager->getRepository('AppBundle:User')->findOneBy(array('recoveryemail' => $email));
            // Encode the password (you could also do this via Doctrine listener)

            if($user)
            {
                $token = bin2hex(openssl_random_pseudo_bytes(10));
                $user->setResetToken($token);
                /* ======== SEND MAIL =========== */
                $msg = \Swift_Message::newInstance()
                    ->setSubject("Reset Password")
                    ->setFrom('thakur.sobhan123@gmail.com')
                    ->setTo($user->getRecoveryemail())
                    ->setBody("Reset your password. Click this link to reset http://localhost:8000/app_dev.php/reset-password/".$token)
                ;

                $this->get('mailer')->send($msg);
                $session->set('resetEmail',$user->getEmail());
                $entityManager->flush();
                return $this->redirectToRoute('login');
            }

            $session->getFlashBag()->set('resetError','Invalid Email');

        }

        return $this->render(
            'AppBundle:User:forgot.html.twig',
            array('form' => $form->createView()));
    }	

    /**
     * @Route("/logout", name="logout")
     */
    public function logoutAction()
    {
    }

    /**
     * @Route("/reset-password/{token}", name="reset_password")
     */
    public function resetPasswordAction($token,Request $request,UserPasswordEncoderInterface $passwordEncoder)
    {
        $session = new Session();
        $updateUser = new User();
        $form = $this->createForm(ResetPassword::class, $updateUser);

        $form->handleRequest($request);

        $entityManager = $this->getDoctrine()->getManager();
        $user = $entityManager->getRepository('AppBundle:User')->findOneBy(array('email' => $session->get('resetEmail'), 'resetToken' => $token));

        if(!$user)
        {
            return new Response("Invalid Link");
        }

        if ($form->isSubmitted()) {
            $match = $this->get('UserServices')->isPasswordSame($user,$updateUser->getPlainPassword());

            if(!$match)
            {
                // Encode the password (you could also do this via Doctrine listener)
                $password = $passwordEncoder->encodePassword($updateUser, $updateUser->getPlainPassword());
                $user->setPassword($password);
                $user->setLoginAttempt(0);
                $user->setResetToken(null);
                $this->get('UserServices')->SetBlockStatus($user,0);
                // save the User!
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($user);
                $entityManager->flush();

                // On Successful registration, redirect to the following path

                return $this->redirectToRoute('login');
            }
            $session->getFlashBag()->set('passwordError','Old and New Password Cannot be Same');
        }


        return $this->render(
            'AppBundle:User:reset.html.twig',
            array('form' => $form->createView()));

        $session->remove('resetEmail');
    }

}