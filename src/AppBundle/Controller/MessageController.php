<?php
namespace AppBundle\Controller;

use AppBundle\Entity\Messages;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class MessageController extends Controller
{
    /**
     * @Route("/create-message", name="create_message")
     */
    public function createMessageAction(Request $request)
    {
        $currentuser = $this->get('security.token_storage')->getToken()->getUsername();

        $session = new Session();
        $session->set('username', $currentuser);

        $form = $this->createFormBuilder()
            ->add('to', TextType::class, array('label' => false))
            ->add('message', TextareaType::class, array('label' => false))
            ->add('send', SubmitType::class, array('label' => 'Send'))
            ->add('draft', SubmitType::class, array('label' => 'Draft'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $To = $form->getData()['to'];
            $msg = $form->getData()['message'];
            $recipients = explode(';',$To);

            $message = new Messages();
            $message->setMessage($msg);

            //array of user objects
            foreach ($recipients as $rec)
            {
                $users = $entityManager->getRepository('AppBundle:User')->findOneBy(array('email' => $rec));
                if(!$users){
                    $session->getFlashBag()->add('invalidRecipient','Invalid Recipient '.$rec);
                    return $this->redirectToRoute('create_message');
                }
                $message->addTo($users);
            }


            // save the User!

            $user = $entityManager->getRepository('AppBundle:User')->findOneBy(array('username' => $currentuser));
            $message->setFrom($user);
            if($form->getClickedButton()->getName() == 'draft')
            {
                $message->setDraftStatus(1);
            }

            $entityManager->persist($message);
            $entityManager->flush();

            // On Successful registration, redirect to the following path

            return $this->redirectToRoute('message_layout');
        }
        return $this->render(
            'AppBundle:Messages:createmessage.html.twig',
            array('form' => $form->createView())
        );
    }


    /**
     * @Route("/message-layout", name="message_layout")
     */
    public function messageLayoutAction(Request $request)
    {

        $currentuser = $this->get('security.token_storage')->getToken()->getUsername();
        $entityManager = $this->getDoctrine()->getManager();
        $user = $entityManager->getRepository('AppBundle:User')->findOneBy(array('username' => $currentuser));

        //fetch result from recipient entity
        $recipients = $entityManager->getRepository("AppBundle\Entity\Messages")
            ->createQueryBuilder('m')
            ->select('u.id as userId, m.id as messageId')
            ->innerJoin('m.To', 'u')
            ->where('u.id = :user_id')
            ->setParameter('user_id', $user->getId())
            ->getQuery()
            ->getResult();

        $size = count($recipients);
        $messages=array();
        //fetch messages based on recipient's message Id
        for ($i = 0; $i<$size; $i++)
        {
            $msg = $recipients[$i]['messageId'];
            $temp = $entityManager->getRepository('AppBundle:Messages')->findOneBy(array("id" => $msg, "draftStatus" => 0));
            if($temp != null)
            {
                $messages[$i] = $temp;
            }
        }

        return $this->render(
            'AppBundle:Messages:messageLayout.html.twig',['messages' => $messages]);
    }

    /**
     * @Route("/sent_messages", name="sent_messages")
     */
    public function sentMessagesAction(Request $request)
    {
        $currentuser = $this->get('security.token_storage')->getToken()->getUsername();
        $entityManager = $this->getDoctrine()->getManager();
        $user = $entityManager->getRepository('AppBundle:User')->findOneBy(array('username' => $currentuser));
        $messages = $entityManager->getRepository('AppBundle:Messages')->findBy(array('From' => $user, 'draftStatus' => 0));
        return $this->render(
            'AppBundle:Messages:sentMessages.html.twig',['messages' => $messages]);
    }

    /**
     * @Route("/show_messages/{id}", name="show_messages")
     */
    public function showInboxMessagesAction($id, Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $msg = $entityManager->getRepository('AppBundle:Messages')->findOneBy(array('id' => $id));
        $to = $msg->getTo();
        $To='';
        for($i=0; $i<count($to); $i++)
        {
            $To = $To.$to[$i]->getEmail()."\n";
        }

        $from = $msg->From->getEmail();
        $message = $msg->getMessage();
        return $this->render(
            'AppBundle:Messages:showMessages.html.twig',['To' => $To, 'From' => $from, 'Message' => $message]);
    }

    /**
     * @Route("/drafts", name="drafts")
     */
    public function draftsAction(Request $request)
    {
        $currentuser = $this->get('security.token_storage')->getToken()->getUsername();
        $entityManager = $this->getDoctrine()->getManager();
        $user = $entityManager->getRepository('AppBundle:User')->findOneBy(array('username' => $currentuser));
        $messages = $entityManager->getRepository('AppBundle:Messages')->findBy(array('From' => $user, 'draftStatus' => 1));
        return $this->render(
            'AppBundle:Messages:sentMessages.html.twig',['messages' => $messages]);
    }

}